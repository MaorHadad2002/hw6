#include "Pentagon.h"
#include "MathUtils.h"

Pentagon::Pentagon(std::string nam, std::string col, double len):Shape(nam, col)
{
	length = len;
}

void Pentagon::setLength(double len)
{
	length = len;
}

void Pentagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "length is " << length << std::endl << "area is " << MathUtils::CalPentagonArea(length) << std::endl;
}