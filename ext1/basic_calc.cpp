#include <iostream>
#define ERROR 8200
#define ERROR_MESSAGE "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year."

int add(int a, int b, bool& isValid) {
	if (a + b == ERROR)
	{ //if the result of add is 8200, isValid -> false
		isValid = false;
		std::cerr << ERROR_MESSAGE << std::endl;  
	}
  return a + b;
}

int  multiply(int a, int b, bool& isValid) {
  int sum = 0;
  for(int i = 0; i < b; i++) {
    sum = add(sum, a, isValid); 
	if (!isValid) //stopping here. we don't want any more prints
	{
		return 0;
	}
  };
  return sum;
}

int  pow(int a, int b, bool& isValid) {
  int exponent = 1;
  for(int i = 0; i < b; i++) {
    exponent = multiply(exponent, a , isValid);
  };
  return exponent;
}

/*
	the function prints the result if the valid is true
	input: result - the result to print. isValid - if the result is valid to print
	output:none
*/
void printIfValid(int result, bool& isValid)
{
	if (isValid)
	{
		std::cout << result << "\n" << std::endl;
	}
	isValid = true; //reseting isValid
}

int main(void) {
	bool isValid = true;
	int result = 0;
	
	//checking for add
	result = add(8000, 200, isValid); //invalid
	printIfValid(result, isValid);

	result = add(8000, 300, isValid); //valid
	printIfValid(result, isValid);

	//checking for multiply
	result = multiply(100, 100, isValid); //invalid
	printIfValid(result, isValid);

	result = multiply(500, 20, isValid); //valid
	printIfValid(result, isValid);

	
	//checking for pow
	result = pow(100, 10, isValid); //invalid
	printIfValid(result, isValid);

	result = pow(2, 20, isValid); //valid
	printIfValid(result, isValid);

  getchar();
}